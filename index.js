const express = require("express");
const expressWs = require("express-ws");
const {nanoid} = require("nanoid");
const app = express();
expressWs(app);

app.get("/", (req, res) => {
  res.send("Main page");
});

const activeConnections = {};
const messages = [];

app.ws("/chat", (ws, req) => {
  const id = nanoid();
  console.log("Client connected! id = " + id);
  activeConnections[id] = ws;

  let username;

  ws.on("message", msg => {
    const decodedMessage = JSON.parse(msg);
    switch(decodedMessage.type) {
      case "SET_USERNAME":
        username = decodedMessage.username;
        break;
      case "GET_ALL_MESSAGES":
        ws.send(JSON.stringify({type: "ALL_MESSAGES", messages}));
        break;
      case "CREATE_MESSAGE":
        Object.keys(activeConnections).forEach(connId => {
          const conn = activeConnections[connId];
          messages.push({
              username,
              text: decodedMessage.text
          });
          conn.send(JSON.stringify({
            type: "NEW_MESSAGE",
            message: {
              username,
              text: decodedMessage.text
            }
          }));
        });
        break;
      default:
        console.log("Unknown message type:", decodedMessage.type);
    }
  });

  ws.on("close", msg => {
    console.log("Client disconnected! id =", id);
    delete activeConnections[id];
  });
});

app.listen(8000, () => {
  console.log("Server started at http://localhost:8000");
});